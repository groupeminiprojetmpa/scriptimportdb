import csv
import argparse
import os
import sqlite3
import logging
import datetime
import atexit
from collections import OrderedDict


def lecture(filename):
    with open(filename) as csvfile:
        donnees = []
        dictDonnees = csv.DictReader(csvfile, delimiter=';')
        for row in dictDonnees:
            donnees.append(row)
    return donnees
    

def checkTable(cursor):
    cursor.execute(''' SELECT name FROM sqlite_master WHERE type='table' AND name='SIV' ''')
    rows = cursor.fetchall()
    if len(rows)==0 :
        logging.info("Table SIV doesn't exist : the program tried to create it")
        cursor.execute('''CREATE TABLE SIV(
            adresse_titulaire           TEXT NOT NULL,
            nom                         TEXT NOT NULL,        
            prenom                      TEXT NOT NULL,
            immatriculation             TEXT PRIMARY KEY UNIQUE, 
            date_immatriculation        TEXT NOT NULL,
            vin                         INTEGER NOT NULL,
            marque                      TEXT NOT NULL,
            denomination_commerciale    TEXT NOT NULL,
            couleur                     TEXT NOT NULL,
            carrosserie                 TEXT NOT NULL,
            categorie                   TEXT NOT NULL,
            cylindree                   INTEGER NOT NULL,
            energie                     INTEGER NOT NULL,
            places                      INTEGER NOT NULL,
            poids                       INTEGER NOT NULL,
            puissance                   INTEGER NOT NULL,
            type                        TEXT NOT NULL,
            variante                    TEXT NOT NULL,
            version                     INTEGER NOT NULL
        );''')

def cleanupfunction(connection):
    if connection:
        logging.info("Cloturing to database")
        connection.close()
    logging.info("Program Ended")

def importData(listdonnees, database_name):
    logging.info("Connecting to database")
    connection = sqlite3.connect(database_name)
    logging.info("Connected to database")
    atexit.register(cleanupfunction, connection)
    cursor = connection.cursor()
    logging.info("Checking Table")
    checkTable(cursor)
    logging.info("Inserting/Updating Data")
    putData(listDonnees, cursor)
    logging.info("Comitting changes to database")
    connection.commit()

def checkData(donnees, cursor):
    value = (donnees['immatriculation'], )
    cursor.execute("""SELECT * FROM SIV WHERE immatriculation=?""", value)
    rows = cursor.fetchall()
    if len(rows)==0 :
        return False
    else :
        return True

def insertData(donnees, cursor):
    valeurs = tuple(donnees.values())
    cursor.execute('INSERT INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', valeurs)
    if (cursor.rowcount == 1):
        logging.info("Insertion successful of immat %s", donnees['immatriculation'])
    else:
        logging.warn("Failed insertion of immat %s", donnees['immatriculation'])

def updateData(donnees, cursor):
    listvaleurs = list(donnees.values())
    listvaleurs.append(donnees['immatriculation'])
    valeurs = tuple(listvaleurs)
    cursor.execute('''UPDATE SIV SET adresse_titulaire=?, nom=?, prenom=?, immatriculation=?, date_immatriculation=?, vin=?, marque=?, denomination_commerciale=?, couleur=?, carrosserie=?, categorie=?, cylindree=?, energie=?, places=?, poids=?, puissance=?, type=?, variante=?, version=? WHERE immatriculation=?''', valeurs)
    if (cursor.rowcount == 1):
        logging.info("Update successful of immat %s", donnees['immatriculation'])
    else:
        logging.warn("Failed update of immat %s", donnees['immatriculation'])

def putData(listDonnees, cursor):
    for donnees in listDonnees :
        if(checkData(donnees, cursor)):
            updateData(donnees, cursor)
        else:
            insertData(donnees, cursor)



if __name__ == '__main__':
    logfilename = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S.log')
    logging.basicConfig(filename=logfilename, level=logging.DEBUG)
    parser = argparse.ArgumentParser(description='Importe les données du fichier csv en praramètre dans une base de données')
    parser.add_argument("filepath")
    args = parser.parse_args()

    if(os.path.exists(args.filepath) and os.path.isfile(args.filepath)):
        logging.info("File does exist")
        listDonnees = lecture(args.filepath)
        logging.info("Importing data")
        importData(listDonnees, 'voitures.sqlite3')
    else:
        logging.info("File doesn't exist")
        print("Erreur fichier inexistant")