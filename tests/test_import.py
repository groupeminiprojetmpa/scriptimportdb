import unittest
import csv
import os
import sqlite3
from collections import OrderedDict
from scriptImport import lecture, checkData, checkTable, putData, insertData, updateData

class TestImportFunctions(unittest.TestCase):

    def setUp(self):
        with open("test.csv", 'w') as writefile:
            writefile.write('adresse_titulaire;nom;prenom;immatriculation;date_immatriculation;vin;marque;denomination_commerciale;couleur;carrosserie;categorie;cylindree;energie;places;poids;puissance;type;variante;version\n')
            writefile.write('3822 Omar Square Suite 257 Port Emily, OK 43251;Smith;Jerome;OVC-568;2012-05-03;9780082351764;Williams Inc;Enhanced well-modulated moderator;LightGoldenRodYellow;45-1743376;34-7904216;3462;37578077;32;3827;110;Inc;92-3625175;79266482\n')
        self.connexion = sqlite3.connect('test.sqlite3')

    #test lecture fichier csv
    def test_lecture(self):
        donnees = lecture("test.csv")
        test = OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        self.assertEqual(test, donnees[0])

    #test vérification table déjà existante et sinon on la crée
    def test_checkTable(self):
        cursor = self.connexion.cursor()
        #requête pour savoir si la table SIV existe
        cursor.execute(''' SELECT name FROM sqlite_master WHERE type='table' AND name='SIV' ''')
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==0)
        checkTable(cursor)
        cursor.execute(''' SELECT name FROM sqlite_master WHERE type='table' AND name='SIV' ''')
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==1)
        checkTable(cursor)
        cursor.execute(''' SELECT name FROM sqlite_master WHERE type='table' AND name='SIV' ''')
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==1)

    #test verification données déjà existante(par rapport à l'immatriculation)
    def test_checkData(self):
        cursor = self.connexion.cursor()  
        checkTable(cursor)                      
        test = OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        self.assertFalse(checkData(test, cursor))
        valeurs = tuple(test.values())
        cursor.execute('INSERT INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', valeurs)
        self.assertTrue(checkData(test, cursor))

    #Test d'insert d'une ligne de données
    def test_insertData(self):
        cursor = self.connexion.cursor()
        checkTable(cursor)
        test = OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        insertData(test, cursor)
        cursor.execute('select * from SIV')
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==1)
        test = OrderedDict([('adresse_titulaire', 'Other adresse'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'Autre immat'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        insertData(test, cursor)
        cursor.execute('select * from SIV')
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==2)

    #Test d'update d'une ligne de données
    def test_updateData(self):
        cursor = self.connexion.cursor()
        checkTable(cursor)
        test = OrderedDict([('adresse_titulaire', '3956 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        insertData(test, cursor)
        test = OrderedDict([('adresse_titulaire', '1999 Random Adress'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])
        updateData(test, cursor)
        cursor.execute('select * from SIV')
        data = cursor.fetchone()
        self.assertEqual(data[0], '1999 Random Adress')

    #Test d'update-insert d'une ligne de données
    def test_putData(self):
        #a modifie
        cursor = self.connexion.cursor()
        checkTable(cursor)
        #test si insert correct
        test = [OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])]
        putData(test, cursor)
        valeur = (test[0]['immatriculation'],) 
        cursor.execute('select * from SIV where immatriculation=?', valeur)
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==1)
        #test si update correct
        test = [OrderedDict([('adresse_titulaire', '1999 Random adress'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'),  ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'),  ('version', '79266482')])]
        putData(test, cursor)
        valeur = (test[0]['immatriculation'],) 
        cursor.execute('select * from SIV where immatriculation=?', valeur)
        rows = cursor.fetchall()
        self.assertTrue(len(rows)==1)

    def tearDown(self):
        os.remove("test.csv")
        self.connexion.close()
        os.remove("test.sqlite3")        
